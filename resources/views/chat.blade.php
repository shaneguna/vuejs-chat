@extends('layouts.app')

@section('content')
    <div id="chat-room">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Chat Room
                    <span class="badge pull-right">@{{ usersInRoom.length }}</span>
                    </div>
                    <chat-log :messages="messages"></chat-log>
                    <chat-composer v-on:sent="addMessage"></chat-composer>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection