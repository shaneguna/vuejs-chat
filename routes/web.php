<?php

use App\Events\MessagePosted;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/chat', function () {
    return view('chat');
})->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/messages', function () {
    // fetch messages
    return App\Message::with('user')->get();
})->middleware('auth');


Route::post('/messages', function () {
   //store message
    $user = Auth::user();
    $request_message = request()->get('message');

    $message = $user->messages()->create(['message'=> $request_message]);
    // announce event
    broadcast(new MessagePosted($message, $user))->toOthers();
    return ['status'=>'1'];
})->middleware('auth');
